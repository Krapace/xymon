#!/bin/sh

########
# Variables
########
XYMSRV="ip_serveur_xymon"
XYMON="/usr/lib/xymon/client/bin/xymon"
TESTNAME="arm"                                          # Nom de la sonde afficher dans Hobbit
# Init des variables de départ
COLOR="clear"
MSG=""
HEAD=""
TEMP=$(($(cat /sys/class/thermal/thermal_zone0/temp)/1000))

########
# Put your code here
########
if [ $TEMP -lt 30 ]
        then COLOR="green"
        else
        if [ $TEMP -lt 65 ]
                then COLOR="yellow"
                else COLOR="red"
        fi
fi
HEAD="Température CPU de $MACHINE  est  $TEMP" # Header Msg Xymon
MSG=$(sudo /opt/vc/bin/vcgencmd measure_temp | cut -d"=" -f2 | tr -d "'C")
########
# On envois le tout a Xymon
########

$XYMON $XYMSRV "status $MACHINE.arm $COLOR `date` 
 CPU: `echo $MSG C` 
  