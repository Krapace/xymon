#!/usr/bin/perl -w -s

#Récupère les info de l'état de la connection ADSL
#Les infos se retrouvent dans /tmp/input.log
#TODO fermer la connection et éviter le timeout

use Net::Telnet();
my $t;
my $username='login';
my $password='motdepasse';
$t = new Net::Telnet (Timeout => 3,
                        Prompt=>'/bash\$ $/',
                        Input_log=>'/tmp/input.log');
$t->open("ip_modem_ovh");
if (!$t) {
        print("Connection Impossible");
}
$t->waitfor('/Username :/');
$t->print($username);
$t->waitfor('/Password :/');
$t->print($password);
#$t->login($username, $password);
@lines = $t->cmd('xdsl info');
print @lines;