#!/usr/bin/python

# -*- encoding: utf-8 -*-
'''
First, install the latest release of Python wrapper: $ pip install ovh
'''
import json
import ovh
import sys

params = {}
params['target'] = sys.argv[1]
params['ttl'] = 3600
params['subDomain'] = '{sous-domaine}'
# Instanciate an OVH Client.
# You can generate new credentials with full access to your account on
# the token creation page
client = ovh.Client(
    endpoint='ovh-eu',               # Endpoint of API OVH Europe (List of available endpoints)
    application_key='xxxxxxxxxx',    # Application Key
    application_secret='xxxxxxxxxx', # Application Secret
    consumer_key='xxxxxxxxxx',       # Consumer Key
)

update = client.put('/domain/zone/{zoneName}/record/{id}',
        **params)

result = client.post('/domain/zone/{zoneName}/refresh')

print json.dumps(update, indent=4)
print json.dumps(result, indent=4)