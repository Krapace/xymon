#!/bin/bash
# Script de surveillance de l'ADSL
# * Instructions *


x_XYMON=/usr/lib/xymon/client/bin/xymon
v_COLOR=green
v_SONDE=adsl
. /usr/lib/xymon/client/ext/ip_cable.sh
#
## - Fonctions

read_telnet(){
/usr/lib/xymon/client/ext/telnet.pl
}
read_telnet
time_up=$(cat /tmp/input.log | grep 'Up time' | cut -d":" -f 4-7|tr -d "[:space:]")
bp_up=$(cat /tmp/input.log | grep 'Down/Up'| cut -d " " -f6 | cut -d "/" -f2)
bp_down=$(cat /tmp/input.log | grep 'Down/Up'| cut -d " " -f6 | cut -d "/" -f1)
domain=$( dig sous-domaine.example.com +short)
envoi_xymon() {
$x_XYMON $XYMONSERVERS "status $CLIENTHOSTNAME.$v_SONDE $v_COLOR `date` Connexion OVH
Uptime : $time_up </br>Download : $bp_down kbps </br>Upload : $bp_up kbps </br>IP Numéricable : $ip_NC</br>sous-domaine.example.com : $domain"
}
#
## - Go Go
## - Construction du message

if [[ $bp_down -lt 12000 ]]
        then v_COLOR=yellow
        else v_COLOR=green
fi
if [[ $domain = $ip_NC ]]
        then v_COLOR=green
        else  v_COLOR=yellow domain=$( $domain "Renouvellement en cours"); /usr/bin/python /usr/lib/xymon/client/ext/update.pl $ip_NC
fi
v_XYMON_CONTENT=""

#
## - Envoi

envoi_xymon