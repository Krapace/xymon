# Xymon

Diverses sondes et script a usage personnel


**arm.sh** récupère la température du Raspberry PI
Ajouter xymon dans le groupe vidéo

Avec visudo : xymon ALL=(ALL:ALL) ALL , NOPASSWD: /opt/vc/bin/vcgencmd
 

**telnet.pl** récupère les info du modem OVH et rempli /tmp/input.log

La deconnection est due a un timeout

TODO fermer la connection


**update.py**

Met a jour un sous domaine avec l'API OVH

Donner la nouvelle IP en paramètre

Nécessite de connaitre l'id du sous domaine (voir https://api.ovh.com/console/#/domain)


**adsl.sh**

Affiche les synchro up et down
Affiche l'état de la connection

Nécessite telnet.pl

Nécessite update.py pour la mise à jour du sous domaine

Nécessite ip_cable.sh (qui récupère l'adresse d'un modem cable Red)